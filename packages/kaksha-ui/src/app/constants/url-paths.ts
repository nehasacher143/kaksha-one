export const LOGOUT_URL = '/auth/logout';
export const ADD_BUTTON_ROUTES = ['student', 'teacher'];
export const FETCH_ORG_ENDPOINT = '/api/organization/v1/fetch';
