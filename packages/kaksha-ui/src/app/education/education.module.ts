import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrganizationComponent } from './components/organization/organization.component';
import { SharedImportsModule } from '../shared-imports/shared-imports.module';
import { OrganizationService } from './services/organization.service';

@NgModule({
  declarations: [OrganizationComponent],
  imports: [CommonModule, SharedImportsModule],
  providers: [OrganizationService],
})
export class EducationModule {}
