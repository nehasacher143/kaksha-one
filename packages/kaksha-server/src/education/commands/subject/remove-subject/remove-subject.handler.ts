import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { RemoveSubjectCommand } from './remove-subject.command';
import { SubjectAggregateService } from '../../../aggregates/subject/subject-aggregate.service';

@CommandHandler(RemoveSubjectCommand)
export class RemoveSubjectHandler
  implements ICommandHandler<RemoveSubjectCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: SubjectAggregateService,
  ) {}

  async execute(command: RemoveSubjectCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.removeSubject(uuid);
    aggregate.commit();
    return;
  }
}
