import { ICommand } from '@nestjs/cqrs';
import { SubjectDto } from '../../../entities/subject/subject.dto';

export class AddSubjectCommand implements ICommand {
  constructor(
    public subjectPayload: SubjectDto,
    public readonly clientHttpRequest: any,
  ) {}
}
