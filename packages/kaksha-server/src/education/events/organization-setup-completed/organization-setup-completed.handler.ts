import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { OrganizationService } from '../../entities/organization/organization.service';
import { OrganizationSetupCompletedEvent } from './organization-setup-completed.event';

@EventsHandler(OrganizationSetupCompletedEvent)
export class OrganizationSetupCompletedHandler implements IEventHandler {
  constructor(private readonly org: OrganizationService) {}
  handle(event: OrganizationSetupCompletedEvent) {
    this.org
      .insertOne(event.organization)
      .then(saved => {})
      .catch(error => {});
  }
}
