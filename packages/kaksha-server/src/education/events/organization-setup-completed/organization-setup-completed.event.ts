import { IEvent } from '@nestjs/cqrs';
import { OrganizationDto } from '../../entities/organization/organization.dto';

export class OrganizationSetupCompletedEvent implements IEvent {
  constructor(public readonly organization: OrganizationDto) {}
}
