import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { OrganizationService } from '../../entities/organization/organization.service';
import { OrganizationUpdatedEvent } from './organization-updated.event';

@EventsHandler(OrganizationUpdatedEvent)
export class OrganizationUpdatedHandler implements IEventHandler {
  constructor(private readonly org: OrganizationService) {}
  handle(event: OrganizationUpdatedEvent) {
    this.org
      .updateOne(
        { uuid: event.organization.uuid },
        { $set: event.organization },
      )
      .then(saved => {})
      .catch(error => {});
  }
}
