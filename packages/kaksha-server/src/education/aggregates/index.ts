import { OrganizationAggregateService } from './organization-aggregate/organization-aggregate.service';
import { SubjectAggregateService } from './subject/subject-aggregate.service';
import { QuizAggregateService } from './quiz-aggregate/quiz-aggregate.service';

export const EducationAggregates = [
  SubjectAggregateService,
  OrganizationAggregateService,
  QuizAggregateService,
];
