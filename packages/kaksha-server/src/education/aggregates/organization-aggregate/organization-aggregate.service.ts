import {
  ForbiddenException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { RoleConstants } from '../../../constants/app-strings';
import { TokenCache } from '../../../auth/entities/token-cache/token-cache.entity';
import { OrganizationDto } from '../../entities/organization/organization.dto';
import { OrganizationService } from '../../entities/organization/organization.service';
import { OrganizationUpdatedEvent } from '../../events/organization-updated/organization-updated.event';
import { OrganizationSetupCompletedEvent } from '../../events/organization-setup-completed/organization-setup-completed.event';
import { Organization } from '../../entities/organization/organization.entity';

@Injectable()
export class OrganizationAggregateService extends AggregateRoot {
  constructor(private readonly organization: OrganizationService) {
    super();
  }

  async setupOrganization(payload: OrganizationDto, token: TokenCache) {
    const organizations = await this.organization.find();
    if (organizations.length > 0) {
      throw new ForbiddenException({
        ORGANIZATION_ALREADY_EXISTS: organizations.length,
      });
    }

    if (!this.checkRoles(token)) {
      throw new UnauthorizedException();
    }

    this.apply(
      new OrganizationSetupCompletedEvent({
        ...new Organization(),
        ...payload,
      }),
    );
  }

  async updateOrganization(payload: OrganizationDto, token: TokenCache) {
    const organization = await this.getOrganization();

    if (!this.checkRoles(token)) {
      throw new UnauthorizedException();
    }

    this.apply(
      new OrganizationUpdatedEvent({
        ...organization,
        ...payload,
      } as Organization),
    );
  }

  async getOrganization() {
    const organizations = await this.organization.find();

    if (organizations.length === 0) {
      throw new ForbiddenException({
        ORGANIZATION_FOUND: organizations.length,
      });
    }

    return organizations[0];
  }

  checkRoles(token: TokenCache) {
    if (
      token.roles.some(role => {
        const allowedRoles = [
          RoleConstants.Administrator,
          RoleConstants.OrganizationAdmin,
        ] as string[];
        return allowedRoles.includes(role);
      })
    ) {
      return true;
    }
    return false;
  }

  async fetchOrganizationForRoles(token: TokenCache) {
    if (!token) {
      token = { roles: [] } as TokenCache;
    }

    const organization = await this.getOrganization();

    if (!this.checkRoles(token)) {
      // block private fields
      organization.email = undefined;
    }

    return organization;
  }
}
