import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CqrsModule } from '@nestjs/cqrs';
import { EducationEntities, EducationEntityServices } from './entities';
import { OrganizationController } from './controllers/organization/organization.controller';
import { StandardController } from './controllers/standard/standard.controller';
import { SubjectController } from './controllers/subject/subject.controller';
import { StudentController } from './controllers/student/student.controller';
import { TeacherController } from './controllers/teacher/teacher.controller';
import { EducationCommandHandlers } from './commands';
import { EducationQueryHandlers } from './queries';
import { EducationEventHandlers } from './events';
import { EducationAggregates } from './aggregates';
import { QuizController } from './controllers/quiz/quiz.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature(EducationEntities),
    HttpModule,
    CqrsModule,
  ],
  providers: [
    ...EducationEntityServices,
    ...EducationCommandHandlers,
    ...EducationQueryHandlers,
    ...EducationEventHandlers,
    ...EducationAggregates,
  ],
  exports: [...EducationEntityServices],
  controllers: [
    OrganizationController,
    StandardController,
    SubjectController,
    StudentController,
    TeacherController,
    QuizController,
  ],
})
export class EducationModule {}
