import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class SubjectDto {
  @IsNotEmpty()
  @IsString()
  standard: string;

  @IsNotEmpty()
  @IsString()
  name: string;
}

export class UpdateSubjectDto {
  @IsNotEmpty()
  @IsString()
  uuid: string;

  @IsOptional()
  @IsString()
  standard: string;

  @IsOptional()
  @IsString()
  name: string;
}
