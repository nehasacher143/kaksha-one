import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class QuizDto {
  @IsString()
  @IsNotEmpty()
  title: string;

  @IsString()
  @IsNotEmpty()
  type: string;

  @IsString()
  @IsNotEmpty()
  subject: string;

  @IsString()
  @IsNotEmpty()
  standard: string;

  @IsString()
  @IsOptional()
  createdBy: string;

  @IsString()
  @IsOptional()
  createdOn: string;

  @IsString()
  @IsOptional()
  createdByEmail: string;

  @IsOptional()
  questions: any;

  @IsOptional()
  answer: any;
}

export class UpdateQuizDto {
  @IsString()
  @IsNotEmpty()
  uuid: string;

  @IsString()
  @IsOptional()
  title: string;

  @IsString()
  @IsOptional()
  type: string;

  @IsString()
  @IsOptional()
  subject: string;

  @IsString()
  @IsOptional()
  standard: string;

  @IsString()
  @IsOptional()
  createdBy: string;

  @IsString()
  @IsOptional()
  createdOn: string;

  @IsString()
  @IsOptional()
  createdByEmail: string;

  @IsString()
  @IsOptional()
  questions: any;

  @IsString()
  @IsOptional()
  answer: any;
}
