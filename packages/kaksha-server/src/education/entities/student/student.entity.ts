import { Entity, BaseEntity, ObjectIdColumn, Column, ObjectID } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

@Entity()
export class Student extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  admissionDate: Date;

  @Column()
  leavingDate: Date;

  @Column()
  academicYear: string; // AcademicYear.name

  @Column()
  firstName: string;

  @Column()
  middleName: string;

  @Column()
  lastName: string;

  @Column()
  guardianName: string;

  @Column()
  name: string;

  constructor() {
    super();
    if (!this.uuid) this.uuid = uuidv4();
  }
}
