import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { OrganizationAggregateService } from '../../aggregates/organization-aggregate/organization-aggregate.service';
import { FetchOrganizationQuery } from './fetch-organization.query';

@QueryHandler(FetchOrganizationQuery)
export class FetchOrganizationHandler implements IQueryHandler {
  constructor(private readonly aggregate: OrganizationAggregateService) {}

  async execute(query: FetchOrganizationQuery) {
    return await this.aggregate.fetchOrganizationForRoles(query.token);
  }
}
